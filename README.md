GreenDaoTest
============

## 手順(2015/9/6時点)
### 準備
- 新規アプリを作成(「GreenDaoTest」とする)
- greenDao をclone(https://github.com/greenrobot/greenDAO.git)

### module のインポート
 - File>New>Import Module で「DaoExampleGenerator」を選択する。
 - DaoExampleGenerator と DaoGenerator がモジュールとして追加される。
 - このときモジュール名を好きな名前に変更可能

### generate用出力フォルダを作成
- mkdir で自前作成。
- 今回は「GreenDaoTest/app」配下に「src-gen」とする。

### generate用のコードを書く
- DaoExampleGenerator 内の ExampleDaoGenerator をカスタマイズする。
- 出力先が「DaoExample」になってるので、先ほど作成したフォルダに変更する。
- 今回は「GreenDaoTest/app/src-gen」とする。

### generate実行
- DaoExampleGenerator>Tasks>application>run を実行する。
- 指定したフォルダ内に generate されたソースが出力される。

### generate されたソースを作成アプリのビルドパスに追加
GreenDaoTest/app の build.gradle に以下を追加。

```groovy
sourceSets {
    main {
        manifest.srcFile 'src/main/AndroidManifest.xml'
        java.srcDirs = ['src/main/java', 'src-gen']
        res.srcDirs = ['src/main/res']
    }
}
```

## 参考
- DaoGenerator: 自動生成ツール本体
- DaoExampleGenerator: 自動生成用の自前ソース
- DaoExample: 自動生成されたソース出力先 兼 それを使うサンプル
